<?php
$name="Dima";
$age=20;
$pi=3.14;

$firstArr=["alex", "vova", "tolya"];
$twoArr=["alex", "vova", "tolya", ["kostya", "olya"]];
$threeArr=["alex", "vova", "tolya", ["kostya", "olya", ["gosha", "mila"]]];
$fourArr=[["alex", "vova", "tolya", ["kostya", "olya", ["gosha", "mila"]]]];
?>
<!<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h3>First exercise</h3>
<ul>
    <li>User name: <?php echo $name?></li>
    <li>Age: <?php echo $age?></li>
    <li>Number pi: <?php echo $pi?></li>
</ul>
<h3>Two exercise</h3>
<ul>
    <li><pre>First array: <?php print_r($firstArr) ?></pre></li>
    <li><pre>Two array: <?php print_r($twoArr) ?></pre></li>
    <li><pre>Three array: <?php print_r($threeArr) ?></pre></li>
    <li><pre>Four array: <?php print_r($fourArr) ?></pre></li>
</ul>
</body>
</html>